//index.js
//获取应用实例
const app = getApp()

const page = [
  'pages/index/index',
  'pages/login/login',
  'pages/home/home',
  'pages/community-list/community-list'
]

Page({
  data: {
    page: page,
  },
  onLoad: function () {

  },
})
