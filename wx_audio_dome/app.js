require("/components/test/index");
//app.js
App({
  onLaunch: function () {
    var that = this;
    //调用API从本地缓存中获取数据
    // var logs = wx.getStorageSync('logs') || [];
    // logs.unshift(Date.now());
    // wx.setStorageSync('logs', logs);
    wx.clearStorage();
    wx.clearStorageSync();
    if (!wx.openBluetoothAdapter) {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      });
      return;
    };

    //获取手机系统
    wx.getSystemInfo({
      success: function(data){
        if( data.system.indexOf('iOS') != -1 ){
          that.globalData.system = false;
        }else{
          that.globalData.system = true;
        };
      }
    });
    //开启定时器获取全局播放状态
    this.intervalAudioStatus();
  },
  onHide: function () {
  },
  getUserInfo: function (cb) {
    var that = this
    if (this.globalData.userInfo) {
      typeof cb == "function" && cb(this.globalData.userInfo)
    } else {
      //调用登录接口
      wx.login({
        success: function () {
          wx.getUserInfo({
            success: function (res) {
              that.globalData.userInfo = res.userInfo
              typeof cb == "function" && cb(that.globalData.userInfo)
            }
          })
        }
      })
    }
  },
  loadAndPlay: function(data,callback){
      console.log('加载播放音频');
      var that = this;
      var json = {};
      json.dataUrl = data.dataUrl;
      json.title = data.title;
      json.success = function(event){
        return callback();
      };
      //加载并且播放音频
      wx.playBackgroundAudio(json);
      //弹出加载框
      wx.showLoading({
        title: '加载中',
        mask: true
      });
      var timer = setInterval(function () {
        if (that.globalData.audioState.status == 1) {
          clearInterval(timer);
          timer = null;
          wx.hideLoading();
        };
      }, 10);
  },
  playAudio: function (data,callback) {
    var that = this;
    if( this.globalData.audioState.status == 1){
      //正在播放音频 判断点击音频是不是新音频
      for(var attr in data ){
        if( this.globalData.audioState[attr] != data[attr] ){
          this.stopAudio();
          this.loadAndPlay(data,callback);
          return;
        };
      };
      return;
    };
    if( this.globalData.audioState.status == 2){
      this.loadAndPlay(data,callback);
    }else{
      if(this.globalData.audioState.system){
        console.log('暂停后播放音频(安卓)');
        wx.playBackgroundAudio();        
      }else{
        console.log('暂停后播放音频(iOS)');
        this.loadAndPlay(data,callback);
      };
    };
  },
  pauseAudio: function () {
    console.log('暂停音频');
    if( this.globalData.audioState.status == 0){
      return;
    };
    wx.pauseBackgroundAudio();
  },
  seekAudio: function (num) {
    var number = Number(num);
    if(isNaN(number)){
      return;
    };
    wx.seekBackgroundAudio({
      position: num
    });
  },
  stopAudio: function () {
    console.log('停止音频')
    if( this.globalData.audioState.status == 2){
      return;
    };
    wx.stopBackgroundAudio();
  },
  playAndPause: function (data,callback) {
    switch(this.globalData.audioState.status){
      case 0:
        this.playAudio(data,callback);
        break;
      case 1:
        this.pauseAudio();
        break;
      case 2:
        this.playAudio(data,callback);
        break;
    };
  },
  intervalAudioStatus: function (){
    var that = this;
    this.globalData.interval = setInterval(function () {
      wx.getBackgroundAudioPlayerState({
        success: function (data) {
          that.globalData.audioState = data;
        },
        fail: function (data) {
          console.log('音频获取失败')
        }
      });
      // 具体页面的方法，由传参而来
      that.audioCallback();
      // 当播放到最后的时候让他由暂停转化成停止
      if( that.globalData.audioState.currentPosition == that.globalData.audioState.duration && that.globalData.audioState.currentPosition && that.globalData.audioState.duration){
        that.stopAudio()
      };
    }, 250);
  },
  //定时执行时自定义方法
  audioCallback: function (){},
  //通过该方法修改 audioCallback
  changeAudioCallback: function(fn){
    if(typeof fn != 'function'){
      return;
    };
    clearInterval(this.globalData.interval);
    this.globalData.interval = null;
    this.audioCallback = fn;
    this.intervalAudioStatus();
  },
  globalData: {
    userInfo: null,
    interval: null,
    //true 安卓和其他系统  false iOS 系统
    system: null,
    audioState: {
      status: 2
    }
  }
})