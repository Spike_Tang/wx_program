//map.js
var app = getApp();
Page({
  data:{
    //坐标位置
    des_latitude: 39.915119,
    des_longitude: 116.403963,
    des_name: '天安门',
    markers:[{
      latitude: 39.915119,
      longitude: 116.403963,
      label: {
        content: '天安门',
        color: '#0D9BF2'
      }
    }],
    // 查看路线
    controls: [{
      position: {},
      iconPath: '/image/daohang.png',
      clickable: true
    }]
  },
  onLoad: function(){
    var that = this;
    //初始化 查看线路按钮位置
    wx.getSystemInfo({
      success: function(data){
        that.initBtn(data.windowWidth,data.windowHeight);
      }
    });
  },
  getroute: function(e){
    console.log(e)
    var that = this;
    wx.showLoading({
      title: '定位中',
      mask: true
    })
    wx.getLocation({
      type: 'gcj02', //返回可以用于wx.openLocation的经纬度
      success: function(res) {
        wx.openLocation({
          latitude: that.data.des_latitude,
          longitude: that.data.des_longitude,
          name: that.data.des_name,
          scale: 28,
          success: function(){
            wx.hideLoading();
          }
        });
      }
    });
  },
  initBtn: function(w,h){
    var width = Math.floor(w *0.6);
    var height = width*70/401;
    var left = Math.floor((w - width) / 2);
    var top = Math.floor(h - height - 30);

    var json = {
      width: width,
      height: height,
      left: left,
      top: top
    };

    this.setData({
      controls: [{
        id: 1,
        iconPath: '/image/daohang.png',
        position: json,
        clickable: true
      }]
    });
  }
});