// pages/scenicSpotInfo/scenicSpotInfo.js
// var apiHelper = require('../../utils/api.js');
// var utils = require('../../utils/utils.js');
var app = getApp();

// var progress = true;

Page({
  data: {
    // 基本ID
    id:'',
    pid:'',
    title:'',
    pName:'',
    // 轮播配置
    imgUrls: [
        'http://img02.tooopen.com/images/20150928/tooopen_sy_143912755726.jpg',
        'http://img06.tooopen.com/images/20160818/tooopen_sy_175866434296.jpg',
        'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg'
    ],
    //简介和展开开关
    openbtn: false,
    Introduce:'',
    //音频
    audio: {
      src:'',
      title:''
    },
    audioLoad:0,
    //拖动条 true 没有拖动  fasle 拖动中
    sliderOff: true,  
    //音频开关 true 停止播放了  false 是播放中
    audioOff: true,
    //音频是否以已经准备（src 是否有值,是不是本页音频） 
    // 0 没有载入音频  1 是本页音频  2 不是本页音频
    audioStatus: 0,
    audioInterval:null,
    nowTime: "00:00",
    endTime: "00:00",
    maxTimeNum: 0,
    nowTimeNum: 0, 
    scrollTop: 0,
    // 周围景点
    scenicRecommendListData:[],
    // 解决分享播放页面后  直接进入分享页面
    shareStatus:false
  },

  // toGoBack:function(){
  //   var pages = getCurrentPages();
  //   if( pages && pages.length <=1){
  //     var pid = this.data.pid;
  //     var pName = this.data.pName;
  //     wx.redirectTo({
  //       url:`../../pages/scenicSpotList/scenicSpotList?id=${pid}&name=${pName}`
  //     })
  //     return
  //   }else{
  //     wx.navigateBack({
  //       delta: 1
  //     });
  //   };
  // },
  // //附件景点跳转
  // toNewPage:function(e){
  //   var id = e.currentTarget.dataset.sid;
  //   var name = e.currentTarget.dataset.name;
  //   var pid = this.data.pid;
  //   var pName = this.data.pName;

  //   wx.redirectTo({
  //     url: '../../pages/scenicSpotInfo/scenicSpotInfo?id='+id+'&pid='+pid+'&name='+name+'&pName='+pName+''
  //   });
  // },
  // // 分享
  // onShareAppMessage: function () {
  //   var title = this.data.title;
  //   var id = this.data.id;
  //   var pid = this.data.pid;
  //   var pName = this.data.pName;
  //   return {
  //     title: `${title}`,
  //     path: `pages/scenicSpotInfo/scenicSpotInfo?id=${id}&pid=${pid}&name=${title}&pName=${pName}`,
  //     success: function (res) {
  //       // 分享成功
  //       // wx.showToast({
  //       //   title: '分享成功！',
  //       //   icon: 'success',
  //       //   duration: 1000
  //       // })
  //     },
  //     fail: function (res) {
  //       // 分享失败
  //       // wx.showToast({
  //       //   title: '分享失败！',
  //       //   icon: 'success',
  //       //   duration: 1000
  //       // })
  //     }
  //   }
  // },
  // //获取景点信息
  // initDetail: function () {
  //   var that = this;
  //   apiHelper.paramData.cmd = "ScenicSpotDetailByID";
  //   apiHelper.paramData.param = {
  //     SSId: that.data.id,
  //     Lon: '',
  //     Lat: ''
  //   };
  //   apiHelper.paramData.version = "";
  //   apiHelper.paramData.language = "";
  //   apiHelper.post(apiHelper.paramData, function (res) {
  //     if (res.state == 0) {
  //       var obj = JSON.parse(res.value);
  //       that.setData({
  //         imgUrls:obj.BigImg.split(','),
  //         Introduce:obj.Introduce,
  //         audio:{
  //           src:obj.VoiceUrl,
  //           name: obj.SSName,
  //           author: obj.LanName,
  //           tempSrc:obj.GoogLeLat
  //           // tempSrc:'http://zy.lianjinglx.com/msYjly/mp3/147151236023021.mp3'//测试用
  //         },
  //         maxTimeNum: obj.QRCode,
  //         endTime: utils.formatSeconds(obj.QRCode)
  //       });
  //       //获取周围景点
  //       that.initRecommend(obj.GaoDeLon, obj.GaoDeLat);

  //     } else {
  //       wx.showToast({
  //         title: '没有数据！',
  //         icon: 'success',
  //         duration: 1000
  //       })
  //     }
  //   });
  // },
  // //获取周围景点
  // initRecommend: function (lon, lat) {
  //   var that = this;
  //   apiHelper.paramData.cmd = "WeiXinScenicSpotNear";
  //   apiHelper.paramData.param = {
  //     SId: that.data.id,
  //     Lon: lon,
  //     Lat: lat,
  //     PId: that.data.pid
  //   };
  //   apiHelper.paramData.version = "";
  //   apiHelper.paramData.language = "";
  //   apiHelper.post(apiHelper.paramData, function (res) {
  //     if (res.state == 0) {
  //       var obj = JSON.parse(res.value);
  //       for (var item in obj) {
  //         obj[item].Distance = (parseInt(obj[item].Distance) > 1000 ? (parseInt(obj[item].Distance) / 1000).toFixed(2) + "km" : +parseInt(obj[item].Distance) + "m");
  //       };
  //       that.setData({ scenicRecommendListData: obj });
  //     } else {
  //       // wx.showToast({
  //       //   title: '没有附近景点数据！',
  //       //   icon: 'success',
  //       //   duration: 1000
  //       // })
  //     }
  //   });
  // },
  // // 展开收缩开关
  // setOpen: function (e) {
  //   var key = e.currentTarget.id;
  //   var value = this.data[key];
  //   this.setData({
  //     openbtn: !value
  //   });
  // },

  // 页面数据初始化
  initData: function(id){
    //模拟多页面
    if( id == 1){
      this.setData({
        audio:{
          title: 'see you again',
          src: 'http://yinyueshiting.baidu.com/data2/music/0bb054cfccca36661cc0ed26513bd0a7/261813945/261813857147600128.mp3?xcode=45b1f596ecbeec0939ce60c9b3abe9ef'        
        }
      });      
    }else{
      this.setData({
        audio:{
          title: '泡沫',
          src: 'http://yinyueshiting.baidu.com/data2/music/134375581/14945107241200128.mp3?xcode=5ced5dd890f36e0fc6826868de970894'        
        }
      });        
    };
  },
  // 音频状态初始化
  initPage: function(){
    var audio = app.globalData.audioConsole;
    if( audio.src == undefined && audio.paused == undefined ){
      console.log('没有载入音频');
      this.setData({
        audioStatus:0
      });
      return;
    };
    if( audio.src != this.data.audio.src ){
      console.log('不是本页音频');
      this.setData({
        audioStatus:2
      });
      return;
    };
    if( audio.src == this.data.audio.src ){
      console.log('是本页音频');
      this.setData({
        audioStatus:1
      });
      return;
    };
  },

  //加载
  loadAudio: function(data){
    app.setAudio(data);
    //修改音频状态
    this.setData({
      audioStatus:1
    });
    //传入本页监测函数
    app.changeAudioCallback(this.getAudioStatus);
  },
  //播放暂停
  playAndPause:function(){
    if( this.data.audioStatus != 1 ){
      this.loadAudio(this.data.audio);
    }else{
      if(this.data.audioOff){
        //播放
        app.globalData.audioConsole.play();  
      }else{
        //暂停
        app.globalData.audioConsole.pause();        
      };
    };
    // 只是为了更快切换图标
    this.setData({
      audioOff: !this.data.audioOff
    });
  },
  //实时状态
  getAudioStatus: function(data){
    //判断是否正在拖动进度条
    if(this.data.sliderOff){
      this.setData({
        audioOff: data.paused,
        audioExist: data.src?true:false,
        nowTime: app.formNumberToTime(data.currentTime) || '0:0',
        endTime: app.formNumberToTime(data.duration) || '0:0',
        nowTimeNum: Math.floor(data.currentTime),
        maxTimeNum: Math.floor(data.duration)
      });
    }else{
      this.setData({
        audioOff: data.paused,
        audioExist: data.src?true:false,
        nowTime: app.formNumberToTime(data.currentTime) || '0:0',
        endTime: app.formNumberToTime(data.duration) || '0:0',
        // nowTimeNum: Math.floor(data.currentTime),
        maxTimeNum: Math.floor(data.duration)
      });
    };
  },

  //拖动条
  sliderTouchstart: function(){
    this.setData({
      sliderOff: false
    });
  },
  sliderTouchend: function(){
    var that = this;
    setTimeout(function(){
      that.setData({
        sliderOff: true
      });
    },500)
  },
  sliderTap: function(){
    var that = this;
    this.setData({
      sliderOff: false
    });
    setTimeout(function(){
      that.setData({
        sliderOff: true
      });
    },500);
  },
  sliderChange: function(data){
    if(this.data.audioStatus == 1){
      app.globalData.audioConsole.seek(data.detail.value);
    };
  },

  // 页面状态钩子方法
  onLoad: function (options){
    console.log('页面加载');

    this.initData(options.id);
    this.initPage();

    // 播放本页音频 才传入该页面监听方法 
    if( this.data.audioStatus == 0 ){
      this.loadAudio(this.data.audio);
      return;
    }else if( this.data.audioStatus == 1 ){
      app.changeAudioCallback(this.getAudioStatus);
      return;
    };
  },
  onShow: function() {
    //console.log('页面显示时');
    // 播放本页音频时第一次同步数据
    if( this.data.audioStatus == 1 ){
      this.getAudioStatus(app.globalData.audioConsole);
    };
  },
  onReady: function() {
    //console.log('页面渲染完成时');
    // this.setData({
    //   audioOff:app.globalData.backgroundAudioPlaying
    // });
    // app.globalData.globlPlayFn = this.backStatus;
    // app.globalData.globlPauseFn = this.backStatus;
    // app.globalData.globlStopFn = this.backStop;
    // var pages = getCurrentPages();
    // if(pages.length>1){
    //   this.firstBackPlay();
    // };
  },
  onHide: function() {
    //console.log('页面隐藏时');
  },
  onUnload: function() {
    //console.log('页面卸载时');
    // wx.stopBackgroundAudio();
    // app.globalData.backgroundAudioPlaying = true;
    // app.globalData.globlPlayFn = null;
    // app.globalData.globlPauseFn = null;
    // app.globalData.globlStopFn = null;
    // clearInterval(this.data.audioInterval);
    // this.data.audioInterval = null;
  }
})
