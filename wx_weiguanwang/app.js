//app.js
App({
  onLaunch: function () {
    var that = this;
    // 判断版本
    if (!wx.getBackgroundAudioManager) {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      });
      return;
    };
    // 获取系统信息
    wx.getSystemInfo({
      success: function(e){
        console.log(e)
        that.globalData.windowWidth = e.windowWidth;
        that.globalData.windowHeight = e.windowHeight;
        if( e.system.indexOf('iOS') != -1 ){
          that.globalData.system = 0;
        }else if( e.system.indexOf('Android') != -1 ){
          that.globalData.system = 1;
        }else{
          that.globalData.system = 2;
        };
      }
    });
    // 获取背景播放器
    this.globalData.audioConsole = wx.getBackgroundAudioManager();
    // 初始化音频回调函数
    this.firstInitAudio();
    //开启定时器获取全局播放状态
    this.interval = setInterval(function(){
      that.getAudioStatus();
    },500);
  },
  // 设置音频
  setAudio: function(data){
    var that = this;
    var audio = this.globalData.audioConsole;
    audio.src = data.src;
    audio.title = data.title;

    this.globalData.src = data.src;
    this.globalData.title = data.title;

    // 获取长度以便播放完毕停止了使用
    var timer = setInterval(function(){
      if(!audio.paused){
        that.globalData.duration = audio.duration;
        clearInterval(timer);
        timer = null;
      };
    },100);
  },
  // 自然播放结束后自动设置音频
  setAgainAudio: function(){
    var that = this;
    var audio = this.globalData.audioConsole;
    audio.src = this.globalData.src;
    audio.title = this.globalData.title;
    var timer = setInterval(function(){
      if(!audio.paused){
        audio.pause();
        // 关闭加载提示框（主要兼容iOS）
        wx.hideLoading();

        clearInterval(timer);
        timer = null;
      };
    },100);
  },
  //开启定时器获取全局播放状态
  getAudioStatus: function (){
    var that = this;
    var audio = that.globalData.audioConsole;
    // 当播放到最后的时候让他由暂停转化成停止 使用 audio.onEnded

    // 播放状态，关闭正在加载弹框，备用方法以防onCanplay失效
    if( !audio.paused ){
      wx.hideLoading();
    };
    that.globalData.duration = audio.duration;
    that.globalData.currentTime = audio.currentTime;
    that.globalData.buffered = audio.buffered;
    that.globalData.paused = audio.paused;
    that.globalData.src = audio.src;
    // 具体页面的方法，由传参而来
    that.audioCallback(audio);
  },
  //定时执行时自定义方法
  audioCallback: function (data){},
  //通过该方法修改 audioCallback
  changeAudioCallback: function(fn){
    var that = this;
    if(typeof fn != 'function'){
      return;
    };
    clearInterval(this.interval);
    this.interval = null;
    this.audioCallback = fn;
    this.interval = setInterval(function(){
      that.getAudioStatus();
    },500);
  },
  audioEndCallback: function (){
    console.log(11111)
  },
  //第一次初始化 背景音频组件
  firstInitAudio: function(){
    var that = this;
    var audio = this.globalData.audioConsole;
    audio.onPlay(function(){
      console.log('音频播放');
    });
    audio.onPause(function(){
      console.log('音频暂停');
    });
    audio.onStop(function(){
      console.log('音频停止');
    });
    // 自然结束之后iOS 会清空src 地址
    audio.onEnded(function(){
      console.log('音频自然播放结束');
      console.log(audio.currentTime,audio.duration,audio.src);
      //自然结束后重新加载
      // if(that.globalData.system == 1){
      //   console.log('安卓');
      //   that.setAgainAudio();
      // }else if(that.globalData.system == 0){
      //   console.log('iOS');
      //   that.setAgainAudio();        
      // };
      that.setAgainAudio(); 
      that.audioEndCallback();
    });
    // 测试时 安卓 未执行该方法
    audio.onTimeUpdate(function(){
      // console.log('音频播放进度更新');
    });
    audio.onPrev(function(){
      console.log('iOS 上一曲');
    });
    audio.onNext(function(){
      console.log('iOS 下一曲');
    });
    audio.onError(function(){
      console.log('音频播放错误');
      that.setAgainAudio();
      wx.showToast({
        title: '播放错误',
        mask: true
      });
      setTimeout(function(){
        wx.hideLoading();
      },1000);
    });
    audio.onWaiting(function(){
      console.log('音频播放数据不足停下加载');
      wx.showLoading({
        title: '加载中...',
        mask: false
      });
    });
    // 测试时 iOS 未执行该方法
    audio.onCanplay(function(){
      console.log('音频可以播放但不保证后面刘畅');
      wx.hideLoading();
    });
  },
  //时间格式转换
  formNumberToTime: function(value){
    if( !value && value == undefined && typeof value == 'undefined'){
      return false;
    };
    var number = Number(value);
    var minute = Math.floor(number/60);
    var second = Math.floor(number%60);
    var str = minute + ':' + second;
    return str;
  },
  globalData:{
    // 0 iOS 1 安卓 2 其他
    system: 0,
    audioConsole: null,
    windowWidth: 0,
    windowHeight: 0
  }
})